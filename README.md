Project Name : Virtual Classroom
Group : CSA Group 16

This project involves implementing a Virtual Classroom with shareable whiteboard, live streaming.
The technologies used are :
1. Canvas
2. WebSocket API(socket.io)
3. Nodejs, Expressjs
4. WebRTC Using PeerJS
5. EJS (Embedded JavaScript)
6. uuid

The languages used are:
1. Javascript
2. HTML
3. CSS 

