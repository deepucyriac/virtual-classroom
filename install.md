				Installation
1. Clone the git repository and go to final folder.
2. Open terminal or command prompt here.
3. Type 'npm install' - To install required dependencies.
4. Type 'peerjs --port 3001' - To run the Peer server.
5. Open another terminal or command prompt window while running the above server.
6. Type 'npm start' - To run the main express server.
7. Go to http://localhost:3000/ - To view the application.
