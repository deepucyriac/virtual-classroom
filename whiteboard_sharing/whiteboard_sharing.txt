      REAL TIME SCREEN SHARING ALONG WITH COLLABORATION TOOLS SUCH AS WHITEBOARD

1. WebRTC(Web Real Time Communication)
    
*	It is a technology which enables Web applications and sites to capture and
       optionally stream audio and/or video media, as well as to exchange arbitrary 
       data between browsers without requiring an intermediary.

*	 The Main functions used are "navigator.mediaDevices.getDisplayMedia()" and
 "RTCPeerConnection".


2. WebSocket API
     
*	 It is an advanced technology that makes it possible to open a two-way 
  interactive communication session between the user's browser and a server.

3. Languages Used:

*	     Java
*	     Java Script
*	     HTML 5