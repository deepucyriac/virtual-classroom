				REACT BASED WHITEBOARD
					
1. React:
	The javascript library react is used to implement functionality in the whiteboard.
	The response to the user interactions are created using react.
	React calls the various functions based on the type of user interaction.
	
2. Canvas:
	The HTML element canvas is used to build the whiteboard.
	The builtin functions in canvas allows us to edit the canvas.
	We can interact with the canvas through the canvas API.
	React calls the canvas functions through this API.

3. Languages used:
	HTML is used in react to create the structure of the whiteboard.
	CSS is used to add styling.
	Javascript is used to add functionality.
	
4. NodeJS:
	Nodejs is used to create the backend for the whiteboard.
	It integrates well with the react javascript library. 
		
